import { Injectable } from "@angular/core";
import { SnackBar } from "./models/snack-bar";
import { Subject } from "rxjs";
import { any } from "codelyzer/util/function";

@Injectable({
  providedIn: "root"
})
export class SnackBarService {
  public onChanges: Subject<any> = new Subject<any>();

  constructor() {}

  public show(snackbar: SnackBar): void {
    this.onChanges.next(snackbar);
  }

  public hide(): void {}
}
