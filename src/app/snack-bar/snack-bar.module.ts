import {
  ModuleWithProviders,
  NgModule
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { SnackBarComponent } from './snack-bar.component';
import { SnackBarService } from './snack-bar.service';

@NgModule({
  declarations: [SnackBarComponent],
  exports: [SnackBarComponent],
  imports: [
    CommonModule
  ],
  providers: [SnackBarService],
})
export class SnackBarModule {
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: SnackBarModule,
      providers: [SnackBarService],
    };
  }
}
