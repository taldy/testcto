import {
  ChangeDetectionStrategy,
  Component, OnDestroy,
  OnInit
} from '@angular/core';
import {SnackBar} from './models/snack-bar';
import Timer = NodeJS.Timer;
import {SnackBarService} from './snack-bar.service';

@Component({
  selector: 'app-snack-bar',
  templateUrl: './snack-bar.component.html',
  styleUrls: ['./snack-bar.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SnackBarComponent implements OnInit, OnDestroy {
  public snackBar: SnackBar;
  public open: boolean;
  private timeout: Timer;

  private subscriptions: any[] = [];

  constructor(private snackBarService: SnackBarService) {
  }

  ngOnInit() {
    // missing part onChanges

    this.subscriptions.push(
      this.snackBarService.onChanges
        .subscribe((snackbar: SnackBar) => {
          if (this.open) {
            this.hide();
          }
          this.show(snackbar);
        }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach((s) => s.unsubscribe);
  }

  public get snackBarClasses(): string {
    return `${this.snackBar.position} ${this.snackBar.type} ${this.open ? 'open' : 'hidden'}`;
  }

  public show(snackBar: SnackBar): void {
    if (snackBar.duration) {
      this.timeout = setTimeout(this.hide.bind(this), snackBar.duration);
    }

    this.snackBar = snackBar;
    this.open = true;
  }

  public hide(): void {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }

    this.open = false;
  }

}
