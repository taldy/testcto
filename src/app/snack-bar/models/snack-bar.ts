export class SnackBar {
  public readonly messages: string[];
  public readonly position: Position;
  public readonly type: SnackBarType;
  public readonly duration: number;

  constructor(data: {
    message?: string;
    messages?: string[];
    duration?: number;
    position?: Position;
    type?: SnackBarType;
  }) {
    this.messages = data.messages || [data.message];
    this.duration = data.duration;
    this.position = data.position ? data.position : 'top-center';
    this.type = data.type ? data.type : 'success';
  }
}

type Position =
  | 'top-left'
  | 'top-center'
  | 'top-right'
  | 'bottom-left'
  | 'bottom-center'
  | 'bottom-right';

type SnackBarType = 'success' | 'error';
