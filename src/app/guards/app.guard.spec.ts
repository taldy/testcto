import { TestBed } from "@angular/core/testing";
import { MockStoreModule, MockAction } from "@reibo/ngrx-mock-test";
import { Store } from "@ngrx/store";

import { AppGuard } from "./app.guard";

describe("AppGuard", () => {
  let appGuard: AppGuard;

  beforeEach(() => {
    const initialState = {
      userName: "",
      email: "",
      password: ""
    };

    TestBed.configureTestingModule({
      imports: [MockStoreModule.forRoot("auth", initialState)],
      providers: [AppGuard]
    });

    appGuard = TestBed.get(AppGuard);
  });

  it("should be created", () => {
    expect(appGuard).toBeTruthy();
  });

  describe("canActivate(): ", () => {
    it("should return false if auth information is empty", () => {
      appGuard.canActivate().subscribe(status => expect(status).toEqual(false));
    });

    it("should return true if auth information is not empty", () => {
      const store = TestBed.get(Store);
      store.dispatch(new MockAction({
        userName: "Vasyl",
        email: "vasyl@example.com",
        password: "123456"
      }));

      appGuard.canActivate().subscribe(status => expect(status).toEqual(true));
    });
  });
});
