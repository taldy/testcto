import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import { map } from "rxjs/operators";

import { Store } from "@ngrx/store";
import { AppState } from "../store/state/app.state";
import { AuthState } from "../store/state/auth.state";

@Injectable({
  providedIn: "root"
})
export class AppGuard implements CanActivate {
  constructor(public store: Store<AppState>) {}

  canActivate(): Observable<boolean> {
    return this.store.map(state => state.auth && !!state.auth.password);
  }
}
