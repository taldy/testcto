import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";

@Injectable()
export class AppService {
  constructor(private http: HttpClient) {}

  getRepos(query: string): Observable<any> {
    return this.http.get(
      `https://api.github.com/search/repositories?q=${query}`
    );
  }
}
