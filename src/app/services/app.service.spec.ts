import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { AppService } from './app.service';

describe('AppService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let appService: AppService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ AppService ]
    });

    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    appService = TestBed.get(AppService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });


  it('should be created', () => {
    expect(appService).toBeTruthy();
  });

  describe('getRepos(): ', () => {
    it('should return expected response (called once)', () => {
      const expectedResponse = {
        total_count: 2,
        items: [
          {
            id: 5,
            full_name: 'owner/repo',
          },
          {
            id: 6,
            full_name: 'owner/repo2',
          },
        ],
      };

      appService.getRepos('1').subscribe(
        response => expect(response).toEqual(expectedResponse, 'should return expected response'),
        fail
      );

      const req = httpTestingController.expectOne('https://api.github.com/search/repositories?q=1');
      expect(req.request.method).toEqual('GET');

      req.flush(expectedResponse);
    });

    it('should use query parameter in a requested URL', () => {
      appService.getRepos('8').subscribe(() => {});

      const req = httpTestingController.expectOne('https://api.github.com/search/repositories?q=8');
      expect(req.request.method).toEqual('GET');

      req.flush({});
    });

  });
});
