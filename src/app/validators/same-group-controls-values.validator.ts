import { FormGroup } from "@angular/forms";

export function sameGroupControlsValues(...controls: string[]) {
  return (group: FormGroup): any => {
    const values = controls.map(controlName => group.get(controlName).value);

    for (let i = 1; i < values.length; i++) {
      if (values[i] !== values[0]) {
        return { sameGroupControlsValues: true };
      }
    }

    return null;
  };
}
