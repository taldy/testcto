import { AbstractControl } from "@angular/forms";

export function digitsValidator(control: AbstractControl): any {
  const incorrectSymbols = [];

  control.value.split("").forEach(symbol => {
    if (isNaN(+symbol) && !incorrectSymbols.includes(symbol)) {
      incorrectSymbols.push(symbol);
    }
  });

  return incorrectSymbols.length ? { digits: { incorrectSymbols } } : null;
}
