import { AbstractControl } from "@angular/forms";

export function uniqueValidator(control: AbstractControl): any {
  const notUniqueSymbols = [];

  control.value.split("").forEach((symbol, i) => {
    if (
      control.value.lastIndexOf(symbol) !== i &&
      !notUniqueSymbols.includes(symbol)
    ) {
      notUniqueSymbols.push(symbol);
    }
  });

  return notUniqueSymbols.length ? { unique: { notUniqueSymbols } } : null;
}
