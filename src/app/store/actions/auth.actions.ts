import { Action } from "@ngrx/store";

import { AuthState } from "../state/auth.state";

export const SET_AUTH = "SET_AUTH";

export class SetAuth implements Action {
  readonly type = SET_AUTH;
  constructor(public payload: AuthState) {}
}

export type Action = SetAuth;
