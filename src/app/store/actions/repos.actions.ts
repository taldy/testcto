import { Action } from "@ngrx/store";

// import { ReposState } from '../state/repos.state';

export const SET_REPOS_ITEMS = "SET_REPOS_ITEMS";
export const CLEAR_REPOS_ITEMS = "CLEAR_REPOS_ITEMS";
export const SET_CURRENT_OWNER = "SET_CURRENT_OWNER";

export class SetReposItems implements Action {
  readonly type = SET_REPOS_ITEMS;
  constructor(public payload: any[]) {}
}

export class SetCurrentOwner implements Action {
  readonly type = SET_CURRENT_OWNER;
  constructor(public payload: number) {}
}

export type Action = SetReposItems | SetCurrentOwner;
