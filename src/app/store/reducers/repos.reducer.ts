import { Repository, Owner, ReposState } from "../state/repos.state";
import * as reposActions from "../actions/repos.actions";

const initialState = {
  currentOwnerId: null,
  owners: {},
  items: []
};

export function reducer(
  state = initialState,
  action: reposActions.Action
): ReposState {
  switch (action.type) {
    case reposActions.SET_REPOS_ITEMS: {
      const owners = {};
      const repos = action.payload.map(repo => {
        owners[repo.owner.id] = new Owner(repo.owner);
        return new Repository(repo.id, repo.full_name, repo.owner.id);
      });

      return {
        ...state,
        items: repos,
        owners: { ...state.owners, ...owners }
      };
    }
    case reposActions.SET_CURRENT_OWNER: {
      return {
        ...state,
        currentOwnerId: action.payload
      };
    }
    default: {
      return state;
    }
  }
}

export const getRepositories = (state: ReposState) => state.items;
export const getOwners = (state: ReposState) => state.owners;
export const getCurrentOwnerId = (state: ReposState) => state.currentOwnerId;
