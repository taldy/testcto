import {
  ActionReducerMap,
  createSelector,
  createFeatureSelector
} from "@ngrx/store";

import { AppState } from "../state/app.state";
import { AuthState } from "../state/auth.state";
import { ReposState } from "../state/repos.state";
import * as auth from "./auth.reducer";
import * as repos from "./repos.reducer";

export const appReducer: ActionReducerMap<AppState> = {
  auth: auth.reducer,
  repos: repos.reducer
};

export const getAuth = createFeatureSelector<AuthState>("auth");

export const getReposState = createFeatureSelector<ReposState>("repos");
export const getRepositories = createSelector(
  getReposState,
  repos.getRepositories
);
export const getOwners = createSelector(
  getReposState,
  repos.getOwners
);

export const getRepositoriesWithOwners = createSelector(
  getRepositories,
  getOwners,
  (repos, owners) =>
    repos.map(repo => ({ ...repo, ownerName: owners[repo.ownerId].login }))
);

const getCurrentOwnerId = createSelector(
  getReposState,
  repos.getCurrentOwnerId
);

export const getCurrentOwner = createSelector(
  getOwners,
  getCurrentOwnerId,
  (owners, currentOwnerId) => owners[currentOwnerId]
);
