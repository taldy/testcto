import { AuthState } from "../state/auth.state";
import * as authActions from "../actions/auth.actions";

const initialState = {
  userName: "",
  email: "",
  password: ""
};

export function reducer(
  state = initialState,
  action: authActions.Action
): AuthState {
  switch (action.type) {
    case authActions.SET_AUTH: {
      return { ...action.payload };
    }
    default: {
      return state;
    }
  }
}
