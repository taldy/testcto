export interface AuthState {
  userName: string;
  email: string;
  password: string;
}
