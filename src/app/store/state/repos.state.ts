export class Repository {
  constructor(
    public id: number,
    public fullName: string,
    public ownerId: number
  ) {}
}

export class Owner {
  login: string;
  id: number;
  node_id: string;
  avatar_url: string;
  gravatar_id: string;
  url: string;
  html_url: string;
  followers_url: string;
  following_url: string;
  gists_url: string;
  starred_url: string;
  subscriptions_url: string;
  organizations_url: string;
  repos_url: string;
  events_url: string;
  received_events_url: string;
  type: string;
  site_admin: boolean;

  constructor(raw) {
    Object.keys(raw).forEach(prop => (this[prop] = raw[prop]));
  }
}

export interface ReposState {
  currentOwnerId: number;
  owners: {
    [id: number]: Owner;
  };
  items: Repository[];
}
