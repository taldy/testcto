import { AuthState } from "./auth.state";
import { ReposState } from "../state/repos.state";

export interface AppState {
  auth: AuthState;
  repos: ReposState;
}
