import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { Actions, Effect } from "@ngrx/effects";
import { map, switchMap, catchError } from "rxjs/operators";

import { AppState } from "../state/app.state";

import * as reposActions from "../actions/repos.actions";

@Injectable()
export class AppEffects {
  constructor(private actions$: Actions, private router: Router) {}

  @Effect({ dispatch: false })
  changeCurrentOwner$ = this.actions$
    .ofType(reposActions.SET_CURRENT_OWNER)
    .pipe(
      map(() => {
        return this.router.navigate(["owners"]);
      })
    );
}
