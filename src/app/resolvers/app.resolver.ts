import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
// import { Observable } from 'rxjs/Observable';
import { Store, select } from "@ngrx/store";
import { mergeMap, map, delay, exhaustMap, take } from "rxjs/operators";
import { forkJoin, of, interval, timer } from "rxjs";

import { AppService } from "../services/app.service";
import { AppState } from "../store/state/app.state";

import { getAuth } from "../store/reducers/app.reducer";
import { SetReposItems } from "../store/actions/repos.actions";

@Injectable({
  providedIn: "root"
})
export class AppResolver implements Resolve<any> {
  constructor(private backend: AppService, private store: Store<AppState>) {}

  resolve() {
    return this.store.pipe(select(getAuth)).pipe(
      take(1),
      exhaustMap(auth => {
        return forkJoin(
          timer(3000).pipe(
            mergeMap(() => this.backend.getRepos(auth.password[0]))
          ),
          timer(5000).pipe(
            mergeMap(() => this.backend.getRepos(auth.password[1]))
          )
        );
      }),
      map(([response1, response2]) => {
        this.store.dispatch(
          new SetReposItems([...response1.items, ...response2.items])
        );

        return true;
      })
    );
  }
}
