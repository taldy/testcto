import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { SnackBar } from "../../snack-bar/models/snack-bar";
import { SnackBarService } from "../../snack-bar/snack-bar.service";
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
  ValidationErrors
} from "@angular/forms";
import { digitsValidator } from "../../validators/digits.validator";
import { uniqueValidator } from "../../validators/unique.validator";
import { sameGroupControlsValues } from "../../validators/same-group-controls-values.validator";
import { Store } from "@ngrx/store";
import { AppState } from "../../store/state/app.state";
import * as fromRoot from "../../store/reducers/app.reducer";
import * as authActions from "../../store/actions/auth.actions";
@Component({
  selector: "app-auth-form",
  templateUrl: "./auth-form.component.html",
  styleUrls: ["./auth-form.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AuthFormComponent implements OnInit {
  authForm: FormGroup;

  constructor(
    private snackBarService: SnackBarService,
    private formBuilder: FormBuilder,
    private store: Store<AppState>,
    private router: Router
  ) {}

  ngOnInit() {
    this.authForm = this.formBuilder.group({
      userName: ["", [Validators.required]],
      email: ["", [Validators.required, Validators.email]],
      passwords: this.formBuilder.group(
        {
          password: [
            "",
            [
              Validators.required,
              digitsValidator,
              uniqueValidator,
              Validators.minLength(6)
            ]
          ],
          passwordConfirmation: ["", []]
        },
        {
          validator: sameGroupControlsValues("password", "passwordConfirmation")
        }
      )
    });
  }

  onFormSubmit() {
    if (this.authForm.invalid) {
      this.validateAllFormFields(this.authForm);

      const passwordsGroup = this.authForm.get("passwords");
      const errors = {
        ...(passwordsGroup.get("password").errors || {}),
        ...(passwordsGroup.errors || {})
      };
      const errorMessages = this.getSnackBarErrorMessages(errors);
      if (errorMessages.length) {
        this.showSnackBar(errorMessages);
      }
      return;
    }

    this.store.dispatch(
      new authActions.SetAuth({
        userName: this.authForm.value.userName,
        email: this.authForm.value.email,
        password: this.authForm.value.passwords.password
      })
    );

    this.router.navigate(["repos"]);
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  getErrorMessages(errors: ValidationErrors) {
    const errorMessages = {
      required: "Field is required",
      email: "Should be a valid e-mail",
      digits: "Symbols must be digits",
      unique: "Symbols must be unique",
      minlength: "Password should be at least 6 symbols length",
      sameGroupControlsValues: "Passwords should be the same"
    };

    return Object.keys(errors || {})
      .filter(key => errors[key])
      .map(key => errorMessages[key])
      .join(", ");
  }

  getSnackBarErrorMessages(errors: ValidationErrors) {
    return Object.keys(errors || {})
      .map(key => {
        switch (key) {
          case "digits":
            return `Password contains non digits symbols: ${errors[
              key
            ].incorrectSymbols.join("")}`;
          case "unique":
            return `Password contains not unique symbols: ${errors[
              key
            ].notUniqueSymbols.join("")}`;
          case "sameGroupControlsValues":
            return this.getErrorMessages({ [key]: errors[key] });
          default:
            return null;
        }
      })
      .filter(Boolean);
  }

  private showSnackBar(messages): void {
    this.snackBarService.show(
      new SnackBar({
        messages,
        duration: 30000
      })
    );
  }
}
