import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  Input,
  Output
} from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs";
import { Store } from "@ngrx/store";
import { Repository } from "../../store/state/repos.state";

@Component({
  selector: "app-child-of-child",
  templateUrl: "./child-of-child.component.html",
  styleUrls: ["./child-of-child.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChildOfChildComponent implements OnInit {
  @Input() repositories: Repository[];
  @Output() select: Subject<number> = new Subject<number>();

  constructor() {}

  ngOnInit() {}

  onOwnerClick(ownerId) {
    this.select.next(ownerId);
  }
}
