import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";
import { Observable } from "rxjs/Observable";

import { Store, select} from "@ngrx/store";
import { getCurrentOwner } from "../../store/reducers/app.reducer";
import { AppState } from "../../store/state/app.state";
import { Owner } from "../../store/state/repos.state";

@Component({
  selector: "app-owners",
  templateUrl: "./owners.component.html",
  styleUrls: ["./owners.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OwnersComponent implements OnInit {
  currentOwner$: Observable<Owner>;

  constructor(private store: Store<AppState>) {
    this.currentOwner$ = store.pipe(select(getCurrentOwner));
  }

  ngOnInit() {}
}
