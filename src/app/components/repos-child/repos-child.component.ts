import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  Input,
  Output
} from "@angular/core";
import { Subject } from "rxjs";

import { Repository } from "../../store/state/repos.state";

@Component({
  selector: "app-repos-child",
  templateUrl: "./repos-child.component.html",
  styleUrls: ["./repos-child.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReposChildComponent implements OnInit {
  @Input() repositories: Repository[];
  @Output() select: Subject<number> = new Subject<number>();

  constructor() {}

  ngOnInit() {}
}
