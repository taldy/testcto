import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";

import { Observable } from "rxjs/Observable";
import { Store, select } from "@ngrx/store";

import { AppState } from "../../store/state/app.state";
import { getRepositoriesWithOwners } from "../../store/reducers/app.reducer";
import { SetCurrentOwner } from "../../store/actions/repos.actions";
import { Repository } from "../../store/state/repos.state";

@Component({
  selector: "app-repos",
  templateUrl: "./repos.component.html",
  styleUrls: ["./repos.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReposComponent implements OnInit {
  repositories$: Observable<Repository[]>;

  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.repositories$ = this.store.pipe(select(getRepositoriesWithOwners));
  }

  onOwnerSelected(ownerId) {
    this.store.dispatch(new SetCurrentOwner(ownerId));
  }
}
