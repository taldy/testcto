import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  Input
} from "@angular/core";

import { Owner } from "../../store/state/repos.state";

@Component({
  selector: "app-owners-child",
  templateUrl: "./owners-child.component.html",
  styleUrls: ["./owners-child.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OwnersChildComponent implements OnInit {
  @Input() currentOwner: Owner;
  text: string;

  constructor() {}

  ngOnInit() {
    this.text = JSON.stringify(this.currentOwner, null, 2);
  }
}
